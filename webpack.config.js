var path = require("path");
var WebpackNotifierPlugin = require("webpack-notifier");
var webpack = require("webpack");

var scriptsPath = path.join(__dirname, "src/scripts");

var config = {
	entry: {
		app: path.join(scriptsPath, "app.js"),
		vendors: ["babel-core/polyfill", "react", "react-redux", "redux-thunk"]
	},
	output: {
		path: path.join(__dirname, "build", "js"),
		filename: "[name].js"
	},
	module: {
		loaders: [
			{
				test: /\.js$/,
				exclude: /node_modules|bower_components|vendor/,
				loaders: ["babel-loader"]
			}
		]
	},
	externals: {
		"jquery": "jQuery"
	},
	resolve: {
		extensions: ["", ".js"],
		modulesDirectories: [
			"node_modules",
			path.join(__dirname, "src", "vendor"),
			path.join(__dirname, "src")
		]
	},
	plugins: [
		new WebpackNotifierPlugin(),
		new webpack.HotModuleReplacementPlugin(),
		new webpack.optimize.CommonsChunkPlugin("vendors", "vendors.js", ["app"])
	]
};

module.exports = config;
