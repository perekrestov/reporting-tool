var gulp = require("gulp");
var sourcemaps = require("gulp-sourcemaps");
var babel = require("gulp-babel");
var concat = require("gulp-concat");
var WebpackDevServer = require('webpack-dev-server');
var webpackStream = require('webpack-stream');
var webpack = require('webpack');
var gutil = require("gulp-util");
var webpackConfig = require('./webpack.config.js');
var objectAssign = require("object-assign");
var del = require("del");

var paths = {
    scripts: ['src/**/*.js']
};

gulp.task("default", ["js:build-dev"], () => {});

gulp.task('watch', () => gulp.watch(paths.scripts, ['default']));

gulp.task("js:build", ["js:clean"], function (cb) {
	var config = objectAssign({}, webpackConfig, {
		plugins: (webpackConfig.plugins || []).concat(
			new webpack.optimize.DedupePlugin(),
			new webpack.optimize.UglifyJsPlugin({
				compress: {
					warnings: false
				}
			}),
			new webpack.DefinePlugin({ "process.env.NODE_ENV": JSON.stringify("production") }),
			new webpack.NormalModuleReplacementPlugin(/redux-logger/, "empty-module")
		)
	});

	webpack(config, function (err, stats) {
		if (err) {
			throw new gutil.PluginError("js:build", err);
		}
		gutil.log("[js:build]", stats.toString({
			colors: true
		}));
		cb();
	});
});

gulp.task("js:build-dev", ["js:clean"], function (cb) {
	var config = objectAssign({}, webpackConfig, {
    devtool: "source-map",
  	debug: true,
		plugins: (webpackConfig.plugins || []).concat(
			new webpack.DefinePlugin({ "process.env.NODE_ENV": JSON.stringify("development") }),
			new webpack.NormalModuleReplacementPlugin(/redux-logger/, "empty-module")
		)
	});

	webpack(config, function (err, stats) {
		if (err) {
			throw new gutil.PluginError("js:build", err);
		}
		gutil.log("[js:build]", stats.toString({
			colors: true
		}));
		cb();
	});
});

gulp.task("js:clean", function () {
	return del(["build/js/*"]);
});

gulp.task("js:server", function () {
	var webpackDevConfig = objectAssign({}, webpackConfig, {
		devtool: "source-map",
		debug: true
	});

	new WebpackDevServer(webpack(webpackDevConfig), {
		noInfo: true,
    hot: true,
		stats: {
			colors: true
		},
    publicPath: "/assets/js/",
	}).listen(9090, "localhost", function (err) {
		if (err) {
			throw new gutil.PluginError("js:server", err);
		}
		gutil.log("[js:server]", "http://localhost:9090/");
	});
});
