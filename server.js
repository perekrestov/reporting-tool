var express = require('express');
var app = express();

app.use('/assets/', express.static('build'));
app.use('/', express.static('src'));

app.get('/', function (req, res) {
  res.send('Hello World!');
});

app.get('/api/reports/:reportsGroup', function (req, res) {
  res.send([{id: 1, name: "Report1"}, {id: 2, name: "Report2"},
  {id: 3, name: "Report3"}]);
});

var server = app.listen(3000, function () {
  var host = server.address().address;
  var port = server.address().port;

  console.log('Example app listening at http://%s:%s', host, port);
});
