import {ADD_REPORT, SAVE_REPORT, RECEIVE_REPORTS} from '../actions/report-actions'

let initialState = {author: "Stas"};
export function reportApp(state = initialState, action) {
  switch (action.type) {
    case ADD_REPORT:
      return Object.assign({}, state, {
        adding: true
      });
    case SAVE_REPORT:
      return Object.assign({}, state, {
        adding: false
      });
    case RECEIVE_REPORTS:
      var reps = action.reports;
      return Object.assign({}, state, {
        reports: reps
      });
    default:
      return state;
  }
}
