import {Provider} from 'react-redux';
import React from 'react';
import ReactDOM from 'react-dom';
import {addReport, saveReport, fetchReports} from './actions/report-actions'
import {reportApp} from './reducers/report'
import AppView from './components/appView'
import CreateStore from './store/configureStore'

const store = CreateStore();

store.subscribe(() =>
  console.log(store.getState())
);

let rootElement = document.getElementById('root');
ReactDOM.render(
  (<Provider store={store}>
      <AppView />
  </Provider>),
  rootElement
);

store.dispatch(fetchReports('testGroup')).then(() =>
  console.log(store.getState())
);
