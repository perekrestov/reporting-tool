import {createStore, applyMiddleware} from 'redux';
import thunkMiddleware from 'redux-thunk';
//import createLogger from 'redux-logger';

//const loggerMiddleware = createLogger();

export default function CreateStore() {
  const createStoreWithMiddleware = applyMiddleware(
    thunkMiddleware // lets us dispatch() functions
    //loggerMiddleware // neat middleware that logs actions
  )(createStore);

  let store = createStoreWithMiddleware(reportApp);
  return store;
}
