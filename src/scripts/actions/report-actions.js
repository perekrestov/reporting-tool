export const ADD_REPORT = 'ADD_REPORT';
export const SAVE_REPORT = 'SAVE_REPORT';
export const EDIT_REPORT = 'EDIT_REPORT';
export const REQUEST_REPORTS = 'REQUEST_REPORTS';
export const RECEIVE_REPORTS = 'RECEIVE_REPORTS'

export function addReport(text) {
  return { type: ADD_REPORT, text };
}
export function saveReport(text) {
  return { type: SAVE_REPORT, text };
}

export function editReport(text) {
  return { type: EDIT_REPORT, text };
}

export function requestReports() {
  return {
    type: REQUEST_REPORTS
  };
}

function receiveReports(reportsGroup, json) {
  return {
    type: RECEIVE_REPORTS,
    reports: json,
    receivedAt: Date.now()
  };
}

export function fetchReports(reportsGroup) {

  // Thunk middleware knows how to handle functions.
  // It passes the dispatch method as an argument to the function,
  // thus making it able to dispatch actions itself.

  return function (dispatch) {

    // First dispatch: the app state is updated to inform
    // that the API call is starting.

    dispatch(requestReports(reportsGroup));

    // The function called by the thunk middleware can return a value,
    // that is passed on as the return value of the dispatch method.

    // In this case, we return a promise to wait for.
    // This is not required by thunk middleware, but it is convenient for us.

    return fetch(`http://localhost:3000/api/reports/${reportsGroup}`)
      .then(response => response.json())
      .then(json =>

        // We can dispatch many times!
        // Here, we update the app state with the results of the API call.

        dispatch(receiveReports(reportsGroup, json))
      );

      // In a real world app, you also want to
      // catch any error in the network call.
  };
}
