import 'whatwg-fetch';

export function getReports(reportsGroup) {

  return fetch(`http://localhost:3000/api/reports/${reportsGroup}`)
    .then(response => response.json());
}
